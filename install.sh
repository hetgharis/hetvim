set -e
npm install -g jslint js-beautify jshint

if [ -d ~/.vim ]; then
	rm -r ~/.vim
fi
rm ~/.vimrc || true

git clone https://bitbucket.org/hetgharis/hetvim.git ~/.vim
(cd ~/.vim && git submodule init && git submodule update)
ln -s ~/.vim/.vimrc ~/.vimrc

