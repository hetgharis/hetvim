execute pathogen#infect()
execute pathogen#helptags()
syntax on

" line numbers
set number

" idents
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

filetype plugin indent on
let mapleader = " "
let g:mapleader = " "
map <leader>n :NERDTreeToggle<CR>
map <leader>a :Autoformat<CR>

syntax enable
set background=dark
colorscheme solarized
let g:solarized_termcolors=256

" ctrlp config
let g:ctrlp_map = '<leader>p'
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_custom_ignore = {
        \'dir':  'node_modules'
    \}

" syntastic config
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_javascript_checkers = ['jshint']

" powerline font
let g:airline_powerline_fonts = 1
" keybinding for search highlight reset
" nnoremap <esc> :noh<return><esc>

au BufNewFile,BufRead *.ejs set filetype=html

let g:UltiSnipsExpandTrigger="<c-z>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
"let g:UltiSnipsJumpBackwardTrigger="<c-z>"

set path=.,**
